package ru.detmir.promo.codes;

import java.awt.*;
import java.io.File;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
/**
 * Created by ADSidorova on 17.02.2017.
 */
public class SimpleWindow extends JFrame {
    SimpleWindow(){
        super("Рабочее окно");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try  {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
        }

        JPanel panel = new JPanel(null);

        final JTextField bigField = new JTextField("", 27);
        bigField.setFont(new Font("Dialog", Font.PLAIN, 13));
        bigField.setHorizontalAlignment(JTextField.RIGHT);
        bigField.setEnabled(false);
        bigField.setToolTipText("Поле для ввода файла .csv");
        bigField.setSize(300,20);
        bigField.setLocation(10,10);
        panel.add(bigField);

        JButton button = new JButton("Выбрать файл");
        button.setAlignmentX(CENTER_ALIGNMENT);
        button.setSize(130, 20);
        button.setLocation(330,10);
        panel.add(button);

        final File[] file = new File[1];
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "CSV files (*csv)", "csv");
                fileopen.setFileFilter(filter);
                int ret = fileopen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    file[0] = fileopen.getSelectedFile();
                    bigField.setText(file[0].getPath());
                }
            }
        });


        JButton button1 = new JButton("Загрузить ваучеры");
        button1.setAlignmentX(CENTER_ALIGNMENT);
        button1.setSize(150, 20);
        button1.setLocation(320,40);
        panel.add(button1);

        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PromoIndexer indexer = new PromoIndexer(
                        new PromoConfig(
                                new PromoSolrConfig("http://hbs-solrm01-stage:8983/solr", "detmir-promo", 100), file[0].getPath(), 1, 1));
                indexer.startIndexing();
                JOptionPane.showMessageDialog(null, "Ваучеры успешно загружены");
            }
        });

        getContentPane().add(panel);
        setPreferredSize(new Dimension(500, 200));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        setResizable(false);
    }
}